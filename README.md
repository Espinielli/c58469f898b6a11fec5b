## Learning Vega 2.0
This tries to reproduce the `jobs` example as described in [the Reactive Vega article](http://idl.cs.washington.edu/papers/reactive-vega).

See also min 3:19 in the video.

**It stops working as soon as you select `Any`**...a mistery: in fact a bug, [issue #330](https://github.com/vega/vega/issues/330),  fixed in [v.2.2.0 release](https://github.com/vega/vega/releases/tag/v2.2.0).

**Made search working after [Parametrized examples](http://vega.github.io/vega-editor/?spec=jobs-params) have been made available!**